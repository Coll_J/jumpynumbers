
f = open("jumpy.txt", "w+")

n = 500

q1 = []

for x in range(0,10):
    q1.append(x)

i = 0
res = 0

while True:
    q2 = q1[:]
    q1 = []
    if(i >= n):
        break
    for x in q2:
        f.write('%d\n' % x)
        i+=1
        res += x
        if(i >= n):
            break
        if(x == 0):
            continue
        if(x%10 == 0):
            q1.append(x * 10 + ((x%10) + 1))
        elif(x%10 == 9):
            q1.append(x * 10 + ((x%10) - 1))
        else:
            q1.append(x * 10 + ((x%10) - 1))
            q1.append(x * 10 + ((x%10) + 1))
    
print(res)

# ---------- reference ----------- #
# q2 = q1[:]
# q1 = []
# q1.append(10)
# print('q1: ')
# for x in q1:
#     print("{} ".format(x))

# print('q2: ')
# for y in q2:
#     print("{} ".format(y))